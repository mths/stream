# "Stream" by mths.io#

This is if you need to stream something, but you don't want the huge latency or unknown people finding it that easily.

### How do I get set up? ###

* Check if your brain has enough resources to complete the following.
* Get a CLEAN debian server. If you don't have any, I would like to suggest a 5USD server from [DigitalOcean](https://www.digitalocean.com/?refcode=a0d1de8ca3a2) (this link gives you 10USD for free, aka. two months!!)
* Login using root
* apt-get update -y && apt-get install -y git dpkg-dev && git clone https://bitbucket.org/mths/stream.git && cd stream/ && chmod +x install.sh && ./install.sh
* Fill in IP or domain name when promted
* Fill in your secret plain text key
* Read instructions

### Who do I talk to if I have problems? ###

* Google