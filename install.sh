echo "Please enter the domain or IP where this server can be reached at (will be used for stream links):"
read rtmp_domain
echo "Please enter the pre shared key you want to use for broadcasting:"
read rtmp_presharedkey
apt-get update
apt-get build-dep nginx -y
mkdir nginx-compile
cd nginx-compile
apt-get source nginx 
git clone https://github.com/arut/nginx-rtmp-module.git
cd nginx-1*
./configure --add-module=../nginx-rtmp-module/
make
make install
cp ../../www /var/. -r
rm /usr/local/nginx/conf/nginx.conf
cp ../../nginx.conf /usr/local/nginx/conf/nginx.conf
cp ../../nginx-init-deb.sh /etc/init.d/nginx
chmod +x /etc/init.d/nginx
/usr/sbin/update-rc.d -f nginx defaults
sed -i -e "s/REPLACE_site_domain/$rtmp_domain/g" /var/www/index.html
sed -i -e "s/REPLACE_presharedkey/$rtmp_presharedkey/g" /usr/local/nginx/conf/nginx.conf
/etc/init.d/nginx start
echo "Done.. I hope... Check above for errors :)"
echo "Here's what you need to know. Your site is available at http://$rtmp_domain/, it's open for everyone."
echo "The url for broadcasting is rtmp://$rtmp_domain/live, the main stream key is main?psk=$rtmp_presharedkey"
echo "You can use other things than main in the stream key. But the web player is only configured for main - just change this if needed."
echo "You should obviously not share the pre shared key with anyone that are not allowed to broadcast."